package com.hcl.management.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="account")
public class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int accountId;
	
	@Column(name="account_no")
	private Long AccountNo;
	
	@Column(name="account_type")
	private String accountType;
	
	@Column(name="balance")
	private double balance;
	
	@ManyToOne
	private User user;

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public Long getAccountNo() {
		return AccountNo;
	}

	public void setAccountNo(Long accountNo) {
		AccountNo = accountNo;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Account(int accountId, Long accountNo, String accountType, double balance, User user) {
		super();
		this.accountId = accountId;
		AccountNo = accountNo;
		this.accountType = accountType;
		this.balance = balance;
		this.user = user;
	}

	public Account() {
		super();
	}

	
	
}
