package com.hcl.management.dto;


public class AccountResponseDto {

	private Long AccountNo;
	
	private String accountType;
	
	private double balance;

	public AccountResponseDto(Long accountNo, String accountType, double balance) {
		super();
		AccountNo = accountNo;
		this.accountType = accountType;
		this.balance = balance;
	}

	public AccountResponseDto() {
		super();
	}

	public Long getAccountNo() {
		return AccountNo;
	}

	public void setAccountNo(Long accountNo) {
		AccountNo = accountNo;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}



}
