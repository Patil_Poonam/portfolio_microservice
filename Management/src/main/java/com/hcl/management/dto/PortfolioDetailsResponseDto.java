package com.hcl.management.dto;



public class PortfolioDetailsResponseDto {
	
	private PurchaseResponseDto purchaseResponseDto;
	
	private double currentPrice;

	public PurchaseResponseDto getPurchaseResponseDto() {
		return purchaseResponseDto;
	}

	public void setPurchaseResponseDto(PurchaseResponseDto purchaseResponseDto) {
		this.purchaseResponseDto = purchaseResponseDto;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public PortfolioDetailsResponseDto(PurchaseResponseDto purchaseResponseDto, double currentPrice) {
		super();
		this.purchaseResponseDto = purchaseResponseDto;
		this.currentPrice = currentPrice;
	}

	public PortfolioDetailsResponseDto() {
		super();
	}
	
	

}
