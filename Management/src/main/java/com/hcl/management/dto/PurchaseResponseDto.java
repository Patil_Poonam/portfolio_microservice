package com.hcl.management.dto;



public class PurchaseResponseDto {

	private String name;

	private int quantity;
	
	private double price;
	
	private String dateTime;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public PurchaseResponseDto(String name, int quantity, double price, String dateTime) {
		super();
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.dateTime = dateTime;
	}

	public PurchaseResponseDto() {
		super();
	}
	
	
	
}
