package com.hcl.portfolio.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioNotFoundException;
import com.hcl.portfolio.service.PortfolioService;


@ExtendWith(MockitoExtension.class)
public class PortfolioControllerTest {

	
	
	@Mock
	PortfolioService portfolioService;
	
	@InjectMocks
	PortfolioController portfoliocontroller;
	
	static PortfolioResponseDto portfolioresponsedto;
	
	@BeforeAll
	public static void setUp() {
		
	
	}
	
	
	@Test
	@DisplayName("Get Portfolios By Id : Positive Scenario")
	public void GetPortfolioById1() throws PortfolioNotFoundException {
		
	 //given
	 when(portfolioService.findById(1)).thenReturn(portfolioresponsedto);
		
	
	//when
	 ResponseEntity<?>  result = portfoliocontroller.findById(1);
			 
	//outcome
	 assertEquals(portfolioresponsedto,result.getBody());
	 assertEquals(HttpStatus.OK, result.getStatusCode());
	
	}
	
	
	@Test
	@DisplayName("Get Portfolio By Id : Negative Scenario")
	public void GetPortfolioById2() throws PortfolioNotFoundException{
		
		//given
		when(portfolioService.findById(1)).thenThrow(PortfolioNotFoundException.class);
		
		//outcome
		assertThrows(PortfolioNotFoundException.class,()->portfoliocontroller.findById(1));
	}


   @Test
   @DisplayName("Get Portfolio By Name : Positive Scenario")
   public void GetPortfolioByName1() throws PortfolioNotFoundException {
	   //given
	   when(portfolioService.findByName("HCL")).thenReturn(portfolioresponsedto);
	   
	  //when
	   ResponseEntity<?>  result1 = portfoliocontroller.findByName("HCL");
	   
	   //outcome
	   assertEquals(portfolioresponsedto,result1.getBody());
	   assertEquals(HttpStatus.OK, result1.getStatusCode());
	   
   }
   
   @Test
	@DisplayName("Get Portfolio By Name : Negative Scenario")
	public void GetPortfolioByName2() throws PortfolioNotFoundException{
		
		//given
		when(portfolioService.findByName("HCL")).thenThrow(PortfolioNotFoundException.class);
		
		//outcome
		assertThrows(PortfolioNotFoundException.class,()->portfoliocontroller.findByName("HCL"));
	}
	
}
